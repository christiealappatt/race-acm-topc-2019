#!/bin/bash

install_dir="/where/to/install/libraries" #Please fill this out

MC="ON" #optional if you want to compare with MC
ABMC="ON" #optional if you want to compare with ABMC

curr_dir=${PWD}

# Step 1: Install RACE
git clone git@bitbucket.org:essex/race.git
cd race
git checkout v0.2.1.0
mkdir build && cd build
CC=icc CXX=icpc cmake .. -DCMAKE_INSTALL_PREFIX=${install_dir} -DBUILD_SHARED_LIBS="OFF"
make install

# Step 2.1: Install COLPACK
COLPACK_CONFIG=""
if [[ $MC == "ON" || $ABMC == "ON" ]]; then
    cd ${curr_dir}
    # Step 2.1 : Install COLPACK for MC and ABMC
    git clone https://github.com/CSCsw/ColPack.git
    cd ColPack
    git checkout 32520440e5856037df356c0e67026d693a51d66b #The commit used
    cd build/cmake
    mkdir colpack_build
    cd colpack_build
    CC=icc CXX=icpc cmake .. -DCMAKE_INSTALL_PREFIX=${install_dir}
    make install
    mv ${install_dir}/include/ColPack_headers ${install_dir}/include/ColPack
    COLPACK_CONFIG="-DGHOST_USE_COLPACK=ON -DCOLPACK_INCLUDE_DIR=${install_dir}/include/ColPack -DCOLPACK_LIBRARIES=${install_dir}/lib/shared_library/libColPack.so"
fi

# Step 2.2: Install METIS
METIS_CONFIG=""
if [[ $ABMC == "ON" ]]; then
    cd ${curr_dir}
    wget "http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/metis-5.1.0.tar.gz"
    tar -xvzf metis-5.1.0.tar.gz
    cd metis-5.1.0
    make config openmp=1 cc=icc prefix=${install_dir}
    cd build/*
    cmake . -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_C_FLAGS=-fPIC
    make install
    METIS_CONFIG="-DGHOST_USE_METIS=ON -DMETIS_INCLUDE_DIR=${install_dir}/include"
fi

cd ${curr_dir}
# Step 2.3: Install GHOST
git clone git@bitbucket.org:essex/ghost.git
cd ghost
git checkout e8e536a
mkdir build && cd build
CC=icc CXX=icpc cmake .. -DCMAKE_INSTALL_PREFIX=${install_dir} -DGHOST_AUTOGEN_KACZ="1,1,1" -DGHOST_AUTOGEN_SPMMV="1,1" -DGHOST_USE_RACE="ON" -DRACE_INCLUDE_DIR="${install_dir}/include" -DRACE_LIBRARIES="${install_dir}/lib/RACE/libRACE.a" -DGHOST_USE_SPMP="ON" ${COLPACK_CONFIG} ${METIS_CONFIG}
make install

cd ${curr_dir}
# Step 3: Install GHOST-Sandbox
git clone git@bitbucket.org:essex/ghost-sandbox.git
cd ghost-sandbox
git checkout origin/RACE
git submodule update --init --recursive
cd ghost-apps
git checkout RACE
cd -
mkdir build && cd build/
CC=icc CXX=icpc cmake .. -DGHOST_DIR=${install_dir}/lib/ghost
cd RACE
make
