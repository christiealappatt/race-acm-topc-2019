# RACE: Artifact Description of ACM TOPC paper #

This is the Artifact Description repository for the ACM-TOPC paper on [RACE](https://arxiv.org/abs/1907.06487).

## How to Install ##
We used RACE with GHOST as front-end to perform computations.
An install script can be found [here](https://bitbucket.org/christiealappatt/race-acm-topc-2019/src/master/install.sh). It installs RACE, GHOST and all the dependencies of GHOST.
In general following three steps have to be followed.

### Step 1 : Install RACE ###
The RACE library can be found [here](https://bitbucket.org/essex/race/src/master/). Steps to install them are:

* `git clone git@bitbucket.org:essex/race.git`
* `cd race && mkdir build && cd build`
* `CC=$(C\_COMPILER) CXX=$(CXX\_COMPILER) cmake ..`
* Configure the library using `ccmake .` (if needed)
* `make`
* `make install`
* Library Dependencies : [hwloc](https://www.open-mpi.org/projects/hwloc/) and [Intel SpMP](https://github.com/IntelLabs/SpMP) will be cloned and installed if not found.

If you just want to try RACE you could skip other steps and proceed to stand-alone examples section of RACE, which could be found [here](https://bitbucket.org/essex/race/src/master/examples/).

### Step 2 : Install GHOST ###
We use [GHOST](https://bitbucket.org/essex/ghost/src/devel/) as frontend to compute, RACE has been integrated to GHOST. Following steps are required to install GHOST.

* `git clone git@bitbucket.org:essex/ghost.git && cd ghost`
* `git checkout e8e536a` #This is the RACE branch used.
* `mkdir build && cd build`
* `CC=icc CXX=icpc cmake .. -DGHOST_USE_SPMP="ON"`
* Use `ccmake .` to configure. If other coloring approaches MC and ABMC have to be tested set `GHOST_USE_COLPACK` and `GHOST_USE_METIS` to ON. The corresponding libraries [ColPack](https://github.com/CSCsw/ColPack) and [METIS](http://glaros.dtc.umn.edu/gkhome/metis/metis/overview) have to be installed and linked.
* make install

### Step 3: Setup GHOST-Sandbox ###
The benchmarks for GHOST are written in [ghost-sandbox](https://bitbucket.org/essex/ghost-sandbox/src/devel/).

* `git clone git@bitbucket.org:essex/ghost-sandbox.git && cd ghost-sandbox`
* `git checkout origin/RACE`
* `git submodule update --init --recursive && cd ghost-apps`
* `git checkout RACE`
* `cd - && mkdir build &&cd build`
* `CC=icc CXX=icpc cmake .. -DGHOST_DIR=${install_dir}/lib/ghost`
* `cd RACE`
* `make`

## Running SymmSpMV benchmark ##

* cd ghost-sanbox/build/RACE
* For example to run symm_spmv with 10 threads, \epsilon_0 = 80, \epsilon_1 = 80, 100 iterations and with RCM pre-permutation the following command can be used:
```
 THREADS=10
 RACE_EFFICIENCY=80,80 GHOST_TASK=disable OMP_SCHEDULE=static OMP_NUM_THREADS=$THREADS GHOST_COLOR_DISTANCE=2 numactl --membind=0 --physcpubind=0-$((THREADS-1)) ./symm_spmv_test_w_r_buf -m "<matrix-market file>" -f SELL-1-1 -c $THREADS -t 1 -w 1 -i 100 --RCM
```
* For using MC or ABMC instead of RACE add the option --MC and --ABMC respectively.
* For running various version of MKL's SymmSpMV  use `symm_spmv_test_w_r_buf_MKL` executable.

## Benchmark matrices ##
Most of the matrices are from the publicly available [SuiteSparse Matrix Collection](https://sparse.tamu.edu). 
We have also included some quantum physics application matrices generated using [ScaMaC library](https://bitbucket.org/essex/matrixcollection/src/master/).
All these matrices generated using ScaMaC and some from SuiteSparse Matrix Collection is available for download [here](https://faubox.rrze.uni-erlangen.de/public?folderID=MllicnhKU0V4eGJmUG14OGpqU0Y3).
